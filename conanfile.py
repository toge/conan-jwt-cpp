import os

from conans import ConanFile, tools

class JwtCppConan(ConanFile):
    name = "jwt-cpp"
    version = "0.4.0"
    license = "MIT"
    author = "toge.mail@gmail.com"
    homepage = "https://thalhammer.it/projects/jwt_cpp"
    url = "https://bitbucket.org/toge/conan-jwt-cpp/"
    description = "A header only library for creating and validating json web tokens in c++"
    topics = ("jwt", "json-web-token", "header only")
    settings = "os", "compiler", "build_type", "arch"
    no_copy_source = True
    requires = "picojson/1.3.0"

    def source(self):
        '''retrieval of the source code here. Remember you can also put the code
        in the folder and use exports instead of retrieving it with this
        source() method
        '''
        tools.get("https://github.com/Thalhammer/jwt-cpp/archive/v{}.zip".format(self.version))
        os.rename("jwt-cpp-{}".format(self.version), "jwt-cpp")
        tools.replace_in_file("jwt-cpp/include/jwt-cpp/jwt.h", "#include \"picojson/picojson.h\"", "#include \"picojson.h\"")

    def package(self):
        self.copy("*.h", "include/jwt-cpp", src="jwt-cpp/include/jwt-cpp")
